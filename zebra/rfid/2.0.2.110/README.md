### What is this repository for? ###

To have the Zebra rfid library v2.0.2.110 in BitBucket.

### How do I get set up? ###
https://maven.apache.org/install.html

### Which command did you use to create the repository? ###
mvn install:install-file -DgroupId=zebra -DartifactId=rfid -Dversion=2.0.2.110 -Dpackaging=aar -Dfile=API3_LIB-release-2.0.2.110.aar -DgeneratePom=true -DlocalRepositoryPath=./rfid_aar_maven_2.0.2.110 -DcreateChecksum=true




